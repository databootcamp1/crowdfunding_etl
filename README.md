# Crowdfunding_ETL  - Project 2
Creator: Melissa Acevedo

## Table of Contents

1. Category and Subcategory DataFrames
2. Campaign DataFrame
3. Contacts DataFrame
4. Crowdfunding Database

### Category and Subcategory DataFrames

1. **Extract and Transform Data:** Create a category DataFrame with "category_id" and "category" columns, where "category_id" ranges from "cat1" to "catn."

2. **Export Data:** Save the category DataFrame as category.csv in the GitHub repository.

**Screenshots:** ![Category DataFrame](images/category_df_screenshot.png)

1. **Extract and Transform Data:** Create a subcategory DataFrame.

2. **Export Data:** Save the subcategory DataFrame as subcategory.csv in the GitHub repository.

**Screenshots:** ![Subcategory DataFrame](images/subcategory_df_screenshot.png)

### Create the Campaign DataFrame

1. **Extract and Transform Data:** Create a campaign DataFrame.

2. **Export Data:** Save the campaign DataFrame as campaign.csv in the GitHub repository.

**Screenshots:** ![Campaign DataFrame](images/campaign_df_screenshot.png)

### Create the Contacts DataFrame

1. **Choose Extraction Method:**
Option 1: Utilize Python dictionary methods.
Option 2: Apply regular expressions.

**Screenshots:** ![Contact DataFrame](images/contact_df_screenshot.png)

### Create the Crowdfunding Database

1. **Inspect Data:** Sketch an ERD using QuickDBD.

**Screenshots:** ![ERD](images/ERD_screenshot.png)

**Table Schema:** Create a table schema for each CSV file.

1. **Database Schema:** Save the schema as crowdfunding_db_schema.sql in the GitHub repository.

2. **Create Database:** Establish a new Postgres database named crowdfunding_db.

3. **Create Tables:** Using the schema, create tables in the correct order to handle foreign keys.

4. **Verification:** Ensure table creation by running SELECT statements for each table.

5. **Import Data:** Import each CSV file into its corresponding SQL table.

6. **Data Verification:** Confirm correct data in each table with SELECT statements.

**Screenshots:**
![Campaign Table](images/campaign_table.png)
![Category Table](images/category_table.png)
![Subcategory Table](images/subcategory_table.png)
![Contacts Table](images/contacts_table.png)